
/**
	special stuff like players shooting
**/

var SPECIAL_ITEMS = new Array();
var SPECIAL_ITEMS_SPEED = 8;
var SPECIAL_ITEMS_LIVETIME = 35;
var SPECIAL_ITEMS_HITSCORE = 50;

function SHOOT() {
	this.player = null;
	this.sprite = null;
	this.speed = null;
	this.time = SPECIAL_ITEMS_LIVETIME;
	
	this.update = function() {
		this.time--;
		if (this.time <= 0)
			this.isDead = true;
		
		this.sprite.x += this.speed.x;
		this.sprite.y += this.speed.y;
		
		updateSpritePosition(this.sprite);
	};
	
	this.onTiles = function() {
	
	//Collision with map:
		var next_tile = getTileOnPosition(this.sprite.x, this.sprite.y);
		if (this.speed.x > 0)
		{
			next_tile += 1;
		} else if (this.speed.x < 0) {
			next_tile -= 1;
		} else if (this.speed.y > 0) {
			next_tile += TILES_X;		
		} else if (this.speed.y < 0) {
			next_tile -= TILES_X;
		}
		
		if (next_tile < 0 || next_tile > 300 || checkForTileNotMovableOnto(next_tile)) 
		{
			this.isDead = true;		
		}
	};
	
	this.collidedWith = null;
	
	this.collision = function()	{
		if (this.collidedWith != null)
		{
			if (this.player != this.collidedWith)
			{
				writeLine("Shoot collided");
				this.collidedWith.life -= 1;
				if (this.collidedWith.life <= 0)
					this.player.highscore += SPECIAL_ITEMS_HITSCORE;
				this.player.highscore += SPECIAL_ITEMS_HITSCORE;
				this.isDead = true;
			}
			this.collideWith = null;
		}
	};
		
	this.isDead = false;
}


function playerShoots(player)
{

	//play sound
	playSound(SOUND_SHOOTING);
	
	//add bullet
		switch(player.direction)
		{
			case PLAYER_DIR_UP:
				addShoot(player, player.sprite.x, player.sprite.y, 0, -SPECIAL_ITEMS_SPEED);
				break;
			case PLAYER_DIR_LEFT:
				addShoot(player, player.sprite.x, player.sprite.y, -SPECIAL_ITEMS_SPEED, 0);
				break;
			case PLAYER_DIR_DOWN:
				addShoot(player, player.sprite.x, player.sprite.y, 0, SPECIAL_ITEMS_SPEED);
				break;
			case PLAYER_DIR_RIGHT:
				addShoot(player, player.sprite.x, player.sprite.y, SPECIAL_ITEMS_SPEED, 0);				
				break;
		}
}

function addShoot(player, xPos, yPos, xVec, yVec)
{
	var shoot = new SHOOT();
	shoot.player = player;
	shoot.speed = new Object();
	shoot.speed.x = xVec;
	shoot.speed.y = yVec;
	
	//calc correct position
	var x = xPos;
	var y = yPos;
	var modx = x % SPECIAL_ITEMS_SPEED;
	var mody = y % SPECIAL_ITEMS_SPEED;
	
	if (xVec > 0)
		x += (SPECIAL_ITEMS_SPEED - modx);
	else if (xVec < 0)
		x -= modx;
	else if (yVec > 0)
		y += (SPECIAL_ITEMS_SPEED - mody);
	else if (yVec < 0)
		y -= mody;
	
	shoot.sprite = addSprite("shoot", x, y, 600, shoot);
	
	shoot.onTiles(); //fix so player can't shoot into a wall
	
	SPECIAL_ITEMS.push(shoot);
}

function clearShoots() {
	SPECIAL_ITEMS = new Array();
}

function cleanupShoots() {
	var SPECIAL_ITEMS_TMP = new Array();
	var SPECIAL_ITEMS = new Array();
	var i;
	
	for (i = 0; i < SPECIAL_ITEMS_TMP; i++)
	{
		var item = SPECIAL_ITEMS_TMP[i];
		
		if (!item.isDead)
		{
			SPECIAL_ITEMS.push(item);	
		}
	}
}
