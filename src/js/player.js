/**
created by Marc Menghin 2006-03-09
**/

//Player object
var PLAYERS = new Array();
var player_statdelayer = 0;

var MAX_PLAYERS = 4;
var PLAYER_DELAYER_COUNT = 10;
var PLAYER_RECHARGE_SPEED = 30;

var PLAYER_DIR_UP = 0;
var PLAYER_DIR_LEFT = 1;
var PLAYER_DIR_DOWN = 2;
var PLAYER_DIR_RIGHT = 3;

var PLAYER_FIRE_NONE = 0;
var PLAYER_FIRE_PRIM = 1;
var PLAYER_FIRE_SEC = 2;

var PLAYER_SPEED_NONE = 0;
var PLAYER_SPEED_NORMAL = 2;
var PLAYER_SPEED_FAST = 4;

var player_human_count = 0;

function PLAYER() {
	this.name = "";
	this.highscore = 0;
	this.life = 3;
	this.item = null;
	this.direction = PLAYER_DIR_UP; //current direction moving
	this.cdirection = PLAYER_DIR_UP; //current direction pressed
	this.fire = PLAYER_FIRE_NONE;
	this.sprite = null;
	this.speed = PLAYER_SPEED_NONE;
	this.cspeed = PLAYER_SPEED_NONE;
	this.id = 0;
	this.collided = false;
	this.recharge = 0;
	
	//Sprite Stuff
	this.update = function() {
		
		if (this.isDead)
			return;
		
		if (this.item != null)
			this.item.update();
			
		//move player:
		if (!this.collided)
		{
		switch(this.direction)
		{
			case PLAYER_DIR_UP:
				this.sprite.y -= this.speed;
				break;
			case PLAYER_DIR_LEFT:
				this.sprite.x -= this.speed;
				break;
			case PLAYER_DIR_DOWN:
				this.sprite.y += this.speed;
				break;
			case PLAYER_DIR_RIGHT:
				this.sprite.x += this.speed;
				break;
		}
		}
		//do shooting
		if (this.fire == PLAYER_FIRE_PRIM)
		{
			if (this.recharge <= 0)
			{
				playerShoots(this);
				this.fire = PLAYER_FIRE_NONE;
				this.recharge = PLAYER_RECHARGE_SPEED;
			} else {
				this.fire = PLAYER_FIRE_NONE;
			}
		} else if (this.fire == PLAYER_FIRE_SEC) {

			if (this.item != null)
				this.item.use();

			this.fire = PLAYER_FIRE_NONE;
		}
		
		this.recharge--;

		this.speed = this.cspeed;
				
		//collide
		if (collidePixel(this))
		{
			
		}
		
		//check if player isn't game over
		if (this.life <= 0)
		{
			this.isDead = true;
			//put out of area
			this.sprite.x = -100;
			this.sprite.y = -100;
			
			//put explosion on that place
			var explosion = new PLAYER_EXPLOSION();
			explosion.sprite = addSprite("player", this.sprite.x, this.sprite.y, 620, explosion);
			playSound(SOUND_EXPLOSION);
		}
		
		updateSpritePosition(this.sprite);
	};
	
	this.onTiles = function() {

		if (this.isDead)
			return;

		switch(this.cdirection) {
			case PLAYER_DIR_UP:
				if (this.direction == PLAYER_DIR_DOWN)
				{
					this.speed = PLAYER_SPEED_NONE;
				} else {
					this.delayer = PLAYER_DELAYER_COUNT;
					this.direction = this.cdirection;
				}
				break;
			case PLAYER_DIR_LEFT:
				if (this.direction == PLAYER_DIR_RIGHT)
				{
					this.speed = PLAYER_SPEED_NONE;
				} else  {
					this.delayer = PLAYER_DELAYER_COUNT;
					this.direction = this.cdirection;
				}
				break;
			case PLAYER_DIR_DOWN:
				if (this.direction == PLAYER_DIR_UP)
				{
					this.speed = PLAYER_SPEED_NONE;
				} else {
					this.delayer = PLAYER_DELAYER_COUNT;
					this.direction = this.cdirection;
				}
				break;
			case PLAYER_DIR_RIGHT:
				if (this.direction == PLAYER_DIR_LEFT)
				{
					this.speed = PLAYER_SPEED_NONE;
				} else  {
					this.delayer = PLAYER_DELAYER_COUNT;
					this.direction = this.cdirection;
				}
				break;
		}
		
		//collision stuff
		if (this.speed != PLAYER_SPEED_NONE && collideTiled(this))
		{
			//writeLine("Player collides");
			this.speed = PLAYER_SPEED_NONE;
			this.collided = true;
		}
		
		
		
			var tileid = ((50 + this.id) * 10 + this.direction);
			
		//if (this.id == 0)
			//writeLine("PlayerTileID: " + tileid + " - speed: " + this.speed);
		
		updateSprite(this.sprite.id, this.sprite.className, this.sprite.x, this.sprite.y, tileid);
	};
	
	this.isDead = false;
};

function PLAYER_EXPLOSION() {
	this.sprite = null;
	this.delay = 8;
	
	//Sprite Stuff
	this.update = function() {
		this.delay--;
		if (this.delay <= 0)
			this.isDead = true;
	};
	
	this.onTiles = function() {
	};
	
	this.isDead = false;
};


function handleInput(inp)
{
      switch(inp) {
			case 13:      //Enter
				break;
			case 16:      //Shift
				break;
			case 17:      //Control
				break;
			case 32:      //Space
				break;
      case 118:   // v
      	PLAYERS[0].fire = PLAYER_FIRE_PRIM;
         break;
      case 98:   // b
      	PLAYERS[0].fire = PLAYER_FIRE_SEC;
         break;
      case 97:   // a
      	PLAYERS[0].cdirection = PLAYER_DIR_LEFT;
    	if (PLAYERS[0].direction == PLAYERS[0].cdirection)
	      	PLAYERS[0].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[0].collided = false;
         break;
			case 100: //d
      	PLAYERS[0].cdirection = PLAYER_DIR_RIGHT;
    	if (PLAYERS[0].direction == PLAYERS[0].cdirection)
	      	PLAYERS[0].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[0].collided = false;
        	
					break;
			case 119: //w
      	PLAYERS[0].cdirection = PLAYER_DIR_UP;			
    	if (PLAYERS[0].direction == PLAYERS[0].cdirection)
	      	PLAYERS[0].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[0].collided = false;
        	
					break;
			case 115: //w
      	PLAYERS[0].cdirection = PLAYER_DIR_DOWN;			
      	    	if (PLAYERS[0].direction == PLAYERS[0].cdirection)
	      	PLAYERS[0].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[0].collided = false;
  			break;
			default: // else
				writeLine("Unknown button for player 1: " + inp);
				break;
  			
		}
		
		if (player_human_count >= 2)
		{
		switch(inp) {
      case 228: // ae
      	PLAYERS[1].fire = PLAYER_FIRE_PRIM;
         break;
      case 35:  // #
      	PLAYERS[1].fire = PLAYER_FIRE_SEC;
         break;
      case 110: // n
      	PLAYERS[1].fire = PLAYER_FIRE_PRIM;
         break;
      case 109: // m
      	PLAYERS[1].fire = PLAYER_FIRE_SEC;
         break;
			case 111: //o
      	PLAYERS[1].cdirection = PLAYER_DIR_UP;			
    	if (PLAYERS[1].direction == PLAYERS[1].cdirection)
	      	PLAYERS[1].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[1].collided = false;

					break;
			case 108: //l
      	PLAYERS[1].cdirection = PLAYER_DIR_DOWN;			
    	if (PLAYERS[1].direction == PLAYERS[1].cdirection)
	      	PLAYERS[1].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[1].collided = false;
      	
					break;
			case 107: //k
      	PLAYERS[1].cdirection = PLAYER_DIR_LEFT;			
    	if (PLAYERS[1].direction == PLAYERS[1].cdirection)
	      	PLAYERS[1].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[1].collided = false;
      	
					break;
			case 246: //oe
      	PLAYERS[1].cdirection = PLAYER_DIR_RIGHT;			
    	if (PLAYERS[1].direction == PLAYERS[1].cdirection)
	      	PLAYERS[1].cspeed = PLAYER_SPEED_NORMAL;
       else
       	PLAYERS[1].collided = false;
					break;
			default: // else
				break;
      }
		}
	}

function getPlayerPositionOnTiles(pos)
{
	return Math.round(pos / TILE_SIZE);
}

function addPlayer(name)
{
	var user = new PLAYER();
	user.name = name;
	user.id = PLAYERS.length;
	
	user.sprite = addSprite("player", (-TILE_SIZE), 0, (((50 + PLAYERS.length) * 10) + user.direction), user); 
	
	PLAYERS.push(user);

	player_human_count++;
	
	return user;
}

function addAIPlayer(name)
{
	var user = new PLAYER();
	user.name = name;
	user.id = PLAYERS.length;
	
	user.sprite = addSprite("player", (-TILE_SIZE), 0, (((50 + PLAYERS.length) * 10) + user.direction), user); 
	
	PLAYERS.push(user);

	return user;
}

function fillMissingPlayersWithAI()
{
	var i;
	
	for (i = player_human_count; i < MAX_PLAYERS; i++)
	{
		addAIPlayer("AI Unit " + (i - player_human_count));
	}
}

function putPlayersOnStartPosition()
{
	var f;
	for (f = 0; f < PLAYERS.length; f++)
	{
		var tile = findMapTile(150 + f);
		
		if (tile != -1)
		{
			var pos = getTilePositionXY(tile);
//			writeLine("Creating Player: " + f + " - " + PLAYERS.length + " - " + tile);
			PLAYERS[f].sprite.x = pos.x * TILE_SIZE;
			PLAYERS[f].sprite.y = pos.y * TILE_SIZE;
			updateSpritePosition(PLAYERS[f].sprite);
			//writeLine("Positioning Player " + f + " on position: " + pos.x + "x - " + pos.y + "y");
		}
	}
}

function initPlayerStats() {

	fillMissingPlayersWithAI();

	var p1 = PLAYERS[0];
	setText(getObject("player1name"), p1.name);

	var p2 = PLAYERS[1];
	setText(getObject("player2name"), p2.name);

	var p3 = PLAYERS[2];
	setText(getObject("player3name"), p3.name);

	var p4 = PLAYERS[3];
	setText(getObject("player4name"), p4.name);
}

function getLifeImg(life)
{
	return "img/life" + life + ".png"
}

function updatePlayerStats() {
	
	player_statdelayer++;
	if (player_statdelayer >= 7)
	{
		player_statdelayer = 0;
	} else {
		return;
	}
	
	var p1 = PLAYERS[0];
	getObject("player1life").style.backgroundImage = "url(" + getLifeImg(p1.life) + ")";
	if (p1.item != null && p1.item.img != null)
		getObject("player1item").style.backgroundImage = "url(" + p1.item.img + ")";
	else
		getObject("player1item").style.backgroundImage = "";
	setText(getObject("player1score"), fixStringSize(p1.highscore, 8, "0"));

	var p2 = PLAYERS[1];
	getObject("player2life").style.backgroundImage = "url(" + getLifeImg(p2.life) + ")";
	if (p2.item != null && p2.item.img != null)
		getObject("player2item").style.backgroundImage = "url(" + p2.item.img + ")";
	else
		getObject("player2item").style.backgroundImage = "";
	setText(getObject("player2score"), fixStringSize(p2.highscore, 8, "0"));

	var p3 = PLAYERS[2];
	getObject("player3life").style.backgroundImage = "url(" + getLifeImg(p3.life) + ")";
	if (p3.item != null && p3.item.img != null)
		getObject("player3item").style.backgroundImage = "url(" + p3.item.img + ")";
	else
		getObject("player3item").style.backgroundImage = "";
	setText(getObject("player3score"), fixStringSize(p3.highscore, 8, "0"));

	var p4 = PLAYERS[3];
	getObject("player4life").style.backgroundImage = "url(" + getLifeImg(p4.life) + ")";
	if (p4.item != null && p4.item.img != null)
		getObject("player4item").style.backgroundImage = "url(" + p4.item.img + ")";
	else
		getObject("player4item").style.backgroundImage = "";
	setText(getObject("player4score"), fixStringSize(p4.highscore, 8, "0"));
}

function clearPlayers() {
	PLAYERS = new Array();
	player_human_count = 0;
}
