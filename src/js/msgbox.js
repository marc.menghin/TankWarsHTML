
var MSGBOX_ID = "msgboxviewpart";
var MSGBOX_DATA_ID = "msgboxdata";
var MSGBOX_CLASSHIDE = "msgboxhide";
var MSGBOX_CLASSVISIBLE = "msgboxshow";
var boxVisible = false;
var runningstatebuffer = false;

function showMSGBox(data)
{
	if (boxVisible)
		return;

	var msgbox = getObject(MSGBOX_ID);
	var msgdata = getObject(MSGBOX_DATA_ID);
	
	msgbox.className = MSGBOX_CLASSVISIBLE;
	msgdata.appendChild(data);
	
	runningstatebuffer = running;
	running = false;
	boxVisible = true;
}

function hideMSGBox()
{
	var msgbox = getObject(MSGBOX_ID);
	msgbox.className = MSGBOX_CLASSHIDE;
	
	var data = getObject(MSGBOX_DATA_ID);
	clearChildNodes(data, "");
	
	running = runningstatebuffer;
	boxVisible = false;
}

function showBox(msgboxid)
{
	if (boxVisible)
		return;
		
	//show box
	var msgbox = getObject(msgboxid);
	msgbox.className = MSGBOX_CLASSVISIBLE;

	//disable buttons
	getObject("startpausebutton").disabled = true;
	getObject("levelbutton").disabled = true;
	getObject("highscorebutton").disabled = true;
	getObject("creditsbutton").disabled = true;
	getObject("helpbutton").disabled = true;

	runningstatebuffer = running;
	running = false;
	boxVisible = true;
}

function hideBox(msgboxid)
{
	//hide box
	var msgbox = getObject(msgboxid);
	msgbox.className = MSGBOX_CLASSHIDE;

	//enable buttons
	getObject("startpausebutton").disabled = false;
	getObject("levelbutton").disabled = false;
	getObject("highscorebutton").disabled = false;
	getObject("creditsbutton").disabled = false;
	getObject("helpbutton").disabled = false;

	running = runningstatebuffer;
	boxVisible = false;
}