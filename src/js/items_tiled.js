
var ITEMS_TILED = new Array();


function TILEDITEM() {
	this.object = null;
	this.sprite = null;
	this.collidedWith = null;
		
	//Sprite Stuff
	this.update = function() {
		this.object.update();
		this.isDead = this.object.isDead;		
	};

	this.collision = function()	{
		this.object.collidedWith = this.collidedWith;
		this.object.collision();
		this.isDead = this.object.isDead;
	};
	
	this.onTiles = function() {
	};
	
	this.isDead = false;
}

function addTiledItem(object)
{
	var ti = new TILEDITEM();
	
	ti.object = object;

	ITEMS_TILED.push(ti);
	
	return ti;
}

function clearTiledItems()
{
	ITEMS_TILED = new Array();
}

function cleanupTiledItems() {
	var ITEMS_TILED_TMP = new Array();
	var ITEMS_TILED = new Array();
	var i;
	
	for (i = 0; i < ITEMS_TILED_TMP; i++)
	{
		var item = ITEMS_TILED_TMP[i];
		
		if (!item.isDead)
		{
			ITEMS_TILED.push(item);	
		}
	}
}

