/**

Bonus Items:

There is one Item that gets picked up and provides a random item to the player.

objItemList

**/
var BONUS_ITEM_COUNT = 0;
var ib_counter = 0;
var IB_MAX = 300;

var ITEM_BONUSITEMBOX = 0;

function ITEM_BOX() {
	this.sprite = null;
	this.collidedWith = null;
	this.img = null;
	this.delayer = 150;

	//Sprite Stuff
	this.update = function() {
		if (this.delayer <= 0)
				this.isDead = true;
			else
				this.delayer--;	
	};

	this.collision = function()	{
		writeLine("BonusCollision: " + this.isDead + " - " + this.collidedWith);
		this.collidedWith.item = getBonusItem();
		
		//add item effect
		this.collidedWith.item.player = this.collidedWith;
		this.collidedWith.item.collidedWith = this.collidedWith;
		this.collidedWith.item.collision();
		
		this.collidedWith = null;
		this.isDead = true;
	};
	
	this.isDead = false;
}

function placeItem() {

	ib_counter++;

	if (ib_counter >= IB_MAX)
	{
		var cur_level = LEVELS[level];
		var k = 0;
		var gotit = false;
	
		while (!gotit && k < 30)
		{
			var num = parseInt((Math.random() * (TILES_Y * TILES_X)) - 1)
			
			if (cur_level[num] <= 109 && cur_level[num] >= 100)
			{
				var pos = getTilePositionXY(num)
				var pixelpos = getTileRectangleInPixel(pos.x, pos.y);
				
				var itemBox = addTiledItem(new ITEM_BOX());
				itemBox.img = getTileImgURL(200);
				
				itemBox.sprite = addSprite("bonusitem", pixelpos.x, pixelpos.y, 200, itemBox);
				
				gotit = true;
			}
		
			k++;
		}
	
		ib_counter = 0;
	} // if (ib_counter >= IB_MAX)
}

function getBonusItem() {
	var itemid = Math.floor(BONUS_ITEM_COUNT * Math.random());
	
	var item = null;
	
	switch(itemid)
	{
		case 0:
			item = new LIFE_UP();
			break;
		case 1:
			item = new LIFE_SPEEDUP();
			break;
		default:
			writeLine("wrong itemid");
			break;
	}
	
	return item;
}
