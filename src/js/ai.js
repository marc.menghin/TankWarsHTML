
var AI_FIRE0 = 0;
var AI_FIRE1 = 1;
var AI_UP = 2;
var AI_LEFT = 3;
var AI_DOWN = 4;
var AI_RIGHT = 5;

function calcAIMovements() {
	var i;
	
	for (i = player_human_count; i < MAX_PLAYERS; i++)
	{
		calcAIMovement(PLAYERS[i]);
	}
}

function calcAIMovement(unit)
{
	var inp = 0;
	var useinp = Math.random() * 100
	
	//shoot everything ^^
	if (useinp <= 60)
	{
		handleUnitInput(AI_FIRE0, unit);
	} else {
		handleUnitInput(AI_FIRE1, unit);
	}

	//use old input

	if (useinp <= 2 || unit.speed == PLAYER_SPEED_NONE)
	{
		//random movement input 
		inp = 2 + Math.floor(Math.random() * 4)
		handleUnitInput(inp, unit);
	} 	
}

function handleUnitInput(inp, unit)
{
      switch(inp) {
      case AI_FIRE0:   // v
      	unit.fire = PLAYER_FIRE_PRIM;
         break;
      case AI_FIRE1:   // b
      	unit.fire = PLAYER_FIRE_SEC;
         break;
      case AI_LEFT:   // a
      	unit.cdirection = PLAYER_DIR_LEFT;
    	if (unit.direction == unit.cdirection)
	      	unit.cspeed = PLAYER_SPEED_NORMAL;
       else
       	unit.collided = false;
         break;
			case AI_RIGHT: //d
      	unit.cdirection = PLAYER_DIR_RIGHT;
    	if (unit.direction == unit.cdirection)
	      	unit.cspeed = PLAYER_SPEED_NORMAL;
       else
       	unit.collided = false;
        	
					break;
			case AI_UP: //w
      	unit.cdirection = PLAYER_DIR_UP;			
    	if (unit.direction == unit.cdirection)
	      	unit.cspeed = PLAYER_SPEED_NORMAL;
       else
       	unit.collided = false;
        	
					break;
			case AI_DOWN: //s
      	unit.cdirection = PLAYER_DIR_DOWN;			
      	    	if (unit.direction == unit.cdirection)
	      	unit.cspeed = PLAYER_SPEED_NORMAL;
       else
       	unit.collided = false;
  			break;
			default: // else
				writeLine("Unknown button for AI Unit: " + inp);
				break;
			}
}
