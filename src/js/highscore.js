
var HIGHSCORES = new Array();
var HS_COOKIENAME = "HighScore_Name_";
var HS_COOKIESCORE = "HighScore_Score_";
var HIGHSCORESCOUNT = 20;

function updateHighScoreTable() {
	readHighScoreTable();
	
	for (i = 0; i < HIGHSCORESCOUNT; i++)
	{
		setText(getObject("highscore_name_" + i), HIGHSCORES[i].name);	
		setText(getObject("highscore_score_" + i), fixStringSize(HIGHSCORES[i].score, 8, "0"));	
	}
}

function addPlayerToHighScore(name, score) {
	readHighScoreTable();
	
	if (HIGHSCORES.length <= 0)
	{
				var winner = new Object();
				winner.name = name;
				winner.score = score;
				
				HIGHSCORES.push(winner);	
				saveHighScoreTable();				
	} else {
		for (i = 0; i < HIGHSCORESCOUNT; i++)
		{
			if (HIGHSCORES[i].score < score)
			{
				var winner = new Object();
				winner.name = name;
				winner.score = score;
				
				HIGHSCORES.splice(i,0, winner);

				saveHighScoreTable();				
				return;
			}
		}
	}
}

function readHighScoreTable() {
	var i;
	
	HIGHSCORES = new Array();
	
	for (i = 0; i < HIGHSCORESCOUNT; i++)
	{
		var winner = new Object();
		var value = loadCookie(HS_COOKIENAME + i);

		if (value != null)
		{
			winner.name = value;
			winner.score = loadCookie(HS_COOKIESCORE + i);
		} else {
			winner.name = "";
			winner.score = "";
		}
		
		HIGHSCORES.push(winner);
	}
}

function saveHighScoreTable() {
	var i;
	
	if (HIGHSCORES == null)
		return;
		
	for (i = 0; i < HIGHSCORESCOUNT; i++)
	{
		storeCookie(HS_COOKIENAME + i, HIGHSCORES[i].name);
		storeCookie(HS_COOKIESCORE + i, HIGHSCORES[i].score);
	}
}

function initHighScoreTable() {
	readHighScoreTable();

	if (HIGHSCORES.length <= 0)
	{
		addPlayerToHighScore("SpOOnY", 2453);
		addPlayerToHighScore("Tri-Gy", 2403);
		addPlayerToHighScore("NoMis", 1576);
		addPlayerToHighScore("dusk",  1315);
		addPlayerToHighScore("D", 762);
		saveHighScoreTable();
	}
}