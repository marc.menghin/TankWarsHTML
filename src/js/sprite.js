
var sprites = new Array();
var spritecount = 0;

function addSprite(className, posX, posY, tileID, actionObj)
{
	var root = getObject(GAME_ROOT_OBJ_ID);
	var node = window.document.createElement("div");
	root.appendChild(node);
	
	//node setup
	node.className = className + String(TILE_SIZE);
	node.id = getElementID("s", spritecount);
	
	node.style.left = String(posX) + "px";
	node.style.top = String(posY) + "px";
	node.style.backgroundImage = "url(" + getTileImgURL(tileID) + ")";
	
	var sprite = new Object();
	sprite.id = spritecount;
	sprite.className = className;
	sprite.node = node;
	sprite.x = posX;
	sprite.y = posY;
	sprite.tileID = tileID;	
	sprite.action = actionObj;
	
	spritecount++;
	sprites.push(sprite);
	
	//printSprites();
	
//	writeLine("SpriteCount: " + sprites.length);
	return sprite;	
}

function getSprite(id)
{
	var i;

	for (i = 0; i < sprites.length; i++)
	{
		if (sprites[i].id == id)
			return sprites[i];
	}
	
	return null;
}

function updateSprite(id, className, posX, posY, tileID)
{
	var sprite = getSprite(id);
	var node = sprite.node;
	
	//node setup
	node.className = className + String(TILE_SIZE);
	node.style.left = String(posX) + "px";
	node.style.top = String(posY) + "px";
	node.style.backgroundImage = "url(" + getTileImgURL(tileID) + ")";
	
	sprite.x = posX;
	sprite.y = posY;
	sprite.tileID = tileID;
	
	return id;	
}

function updateSpritePosition(sprite)
{
	updateSprite(sprite.id, sprite.className, sprite.x, sprite.y, sprite.tileID);
}

/**
	Removed all sprites from the GAME_ROOT_OBJ_ID;
**/
function clearSprites() {
	//HTML Cleanup
	clearChildNodes(getObject(GAME_ROOT_OBJ_ID), "s");
	
	sprites = new Array();
	spritecount = 0;
}


function updateSprites()
{
	var i;
	var sprite;
	var action;
	
	if (!sprites)
		return;
		
//Update & OnTileCheck
	for (i = 0; i < sprites.length; i++)
	{
		sprite = sprites[i];
		action = sprite.action;
		
		action.update();

		if (onTile(sprite.x, sprite.y))
		{
			action.onTiles();
		}
	}
	
//RemoveDead
	var startxxl = (sprites.length - 1);

	for (i = startxxl; i >= 0; i--)
	{
		sprite = sprites[i];
		if (sprite.action.isDead)
		{
			//writeLine("Dead Sprite: " + sprite.node.id);
			clearChildNodesByID(getObject(GAME_ROOT_OBJ_ID), sprite.node.id);

			sprites.splice(i, 1);
		}
	}
}

function printSprites() {
	var result = "";
	var i;
	
	for (i = 0; i < sprites.length; i++)
	{
		if (sprites[i].node)
		{
			result = result + ", " + String(sprites[i].node.id);
		}
	}	
	
	writeLine("Current Sprite IDs: " + result);
}
