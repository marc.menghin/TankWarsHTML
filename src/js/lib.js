
//----------------------------------------------------------------------
// Compatibility layer for different browsers
// mostly to compensate the differenzes between IE and the others

var nav = 0;                // type of browser (0 = unknown, 1 = Netscape, 2 = IE, 3 = Opera) 

function getObject(id) {
	
	var result =  (document.getElementById) ? document.getElementById(id) : document.all(id);
	
	if (!result)
		result = document.getElementsByName(id);
		
	return result;
	
}

function clearChildNodes(root, start)
{
	var childs = root.childNodes;
	var i;
	
	for (i = (childs.length - 1); i >= 0; i--)
	{
		if (String(childs[i].id).indexOf(start) == 0)
			root.removeChild(childs[i]);
	}
}

function clearChildNodesByID(root, idstart)
{
	var childs = root.childNodes;
	var start = (childs.length - 1);
	var i;
	for (i = start; i >= 0; i--)
	{
		if (childs[i].id == idstart)
			root.removeChild(childs[i]);
	}
}

function fixStringSize(string, length, char)
{
	var result = String(string);
	while (result.length < length)
	{
		result = char + result;
	}
	return result;
}

function writeLine(str)
{
	var obj = getObject("debugfield");
	var newstr = window.document.createTextNode(str);
	obj.insertBefore(window.document.createElement("br"), obj.firstChild);
	obj.insertBefore(newstr, obj.firstChild);
}

function setText(obj, str)
{
	var newstr = window.document.createTextNode(str);
	clearChildNodes(obj, "");
	obj.appendChild(newstr);
}

/**
	Returns the ID of a tile on the position. the returned string will start with "txt"
**/
function getElementID(txt, pos) {
	return txt + fixStringSize(pos, 10, 0);
}


function checkBrowser()
{
   // test for browser
   if (navigator.appName == "Netscape") {
      nav=1;
   } else if (navigator.appName == "Microsoft Internet Explorer") {
      nav=2;
      if (navigator.appVersion < 5.5)
      	alert("IE should be at least of version 5.5. You should update or install a different browser like Opera or FireFox. See http://www.opera.com/ or http://www.mozilla.com/ for a download.");
	} else if (navigator.appName == "Opera") {
		nav=3;
   } else {
      alert("Unfortunately, this game requires a Opera, Mozilla (Netscape, FireFox, Epiphany, ...), Safari or Microsoft browser (not recommended).\n\n" +
            "See http://www.opera.com/ or http://www.mozilla.com/ for a download. Your Browser identifies itself as a " +
            navigator.appName) + 
            "\n\nGame will continue but there is no garantie that it will work the way it should.";
   }

   // redirect keyboard events to loop (IE)
   if (nav==1) { 
      window.document.captureEvents(Event.KEYPRESS);
   }
   window.document.onkeypress = keyboardEvent;
}